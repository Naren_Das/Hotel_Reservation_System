<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/1/2017
 * Time: 10:42 PM
 */

namespace App\HotelBooking;

use App\Utility\Utility;
use PDO;
use App\Message\Message;
use App\Model\Database;

class HotelBooking extends Database {
    public $bookingId;
    public $hotelName;
    public $hotelLocation;
    public $singleRoom;
    public $doubleRoom;
    public $suitRoom;
    public $bookingDuration;
    public $contactNo;
    public $bookingTime;
    public $hotelId;

    public function setData($receiveDataArray){

        if(array_key_exists("serialNo", $receiveDataArray)){
            $this->bookingId = $receiveDataArray['serialNo'];
        }

        if(array_key_exists("hotelName",$receiveDataArray)){
            $this->hotelName = $receiveDataArray['hotelName'];
        }

        if(array_key_exists("location",$receiveDataArray)){
            $this->hotelLocation = $receiveDataArray['location'];
        }


        if(array_key_exists("singleRoom",$receiveDataArray)){
            $this->singleRoom = $receiveDataArray['singleRoom'];
        }

        if(array_key_exists("doubleRoom",$receiveDataArray)){
            $this->doubleRoom = $receiveDataArray['doubleRoom'];
        }

        if(array_key_exists("suitRoom",$receiveDataArray)){
            $this->suitRoom = $receiveDataArray['suitRoom'];
        }

        if(array_key_exists("bookingDuration",$receiveDataArray)){
            $this->bookingDuration = $receiveDataArray['bookingDuration'];
        }

        if(array_key_exists("contactNo",$receiveDataArray)){
            $this->contactNo = $receiveDataArray['contactNo'];
        }

        if(array_key_exists("currentDateTime",$receiveDataArray)){
            $this->bookingTime = $receiveDataArray['currentDateTime'];
        }

        if(array_key_exists("hotelId",$receiveDataArray)){
            $this->hotelId = $receiveDataArray['hotelId'];
        }
    }//end of setData()

    public function serailNo(){

        $sqlQuery = "select (ifnull (max(book_id),0)+1) as book_id from tb_booking";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;

    }//end of serialNo()

    public function duplicacyCheck(){

        $sqlQuery = "select * from tb_booking where book_id = $this->bookingId";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        if($singleData){
            return false;
        }
        else{
            return true;
        }

    }//end of duplicacyCheck()

    public function bookingValidation(){

        $sqlQuery = "select book_id from tb_booking where book_id = $this->bookingId";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        if($singleData){
            return false;
        }
        else{
            return true;
        }

    }//end of bookingValidation()

    public function store(){

        $bookId  =$this->bookingId;
        $hotel = $this->hotelName;
        $location = $this->hotelLocation;
        $singleRoom = $this->singleRoom;
        $doubleRoom = $this->doubleRoom;
        $suitRoom = $this->suitRoom;
        $duration = $this->bookingDuration;
        $contact = $this->contactNo;
        $bookTime = $this->bookingTime;
        $hotelIdNo = $this->hotelId;

        $dataArray = array($bookId,$hotel,$location,$singleRoom,$doubleRoom,$suitRoom,$duration,$contact,$bookTime,$hotelIdNo);

        $sqlQuery = "INSERT INTO tb_booking (book_id, book_hotel, hotel_location, single_room, double_room, suit_room, Durations, mobile_no, booking_time, h_ID) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        $STH = $this->DBH->prepare($sqlQuery);
        $result = $STH->execute($dataArray);

        if($result){
            Message::message("Information has been recorded Successfully!");
        }
        else{
            Message::message("Error in Operation!");
        }

    }//end of store()

    public function view(){

        $sqlQuery = "select * from tb_booking where book_id = $this->bookingId";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;

    }//end of view()

    public function priceList(){

        $sqlQuery = "select single_room_price,double_room_price,suit_room_price from tb_hotel INNER JOIN tb_booking on tb_hotel.h_ID = tb_booking.h_ID AND tb_booking.book_id = $this->bookingId";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;

    }//end of priceList()

}//end of HotelBooking