<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 6/30/2017
 * Time: 5:11 PM
 */

namespace App\HotelDetails;

use PDO;
use App\Model\Database;

class HotelDetails extends Database {
    public $id;

    public function setData($receieveDataArray){
        if(array_key_exists("id",$receieveDataArray)){
            $this->id = $receieveDataArray['id'];
        }
    }//end of setData()

    public function index(){

       $sqlQuery = "SELECT * FROM `tb_hotel`";

       $STH = $this->DBH->query($sqlQuery);

       $STH->setFetchMode(PDO::FETCH_OBJ);

       $allData = $STH->fetchAll();

       return $allData;

    }//end of index()

    public function view(){

        $sqlQuery = "select * from tb_hotel where h_ID = $this->id";

        $STH = $this->DBH->query($sqlQuery);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $singleData = $STH->fetch();

        return $singleData;

    }//end of view()

}//end of HotelDetails class