<?php

    namespace App\Utility;


class Utility{

    public static function d($data){
        echo "<pre>";
            var_dump($data);
        echo "</pre>";
    }//end of d()

    public static function dd($data){
        echo "<pre>";
            var_dump($data);
            die();
        echo "</pre>";
    }//end of dd()

    public static function redirect($data){
        header('location:'.$data);
    }//end of redirect()

}//end of Utility class