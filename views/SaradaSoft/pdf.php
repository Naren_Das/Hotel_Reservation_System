<?php
require_once("../../vendor/autoload.php");
use App\HotelBooking\HotelBooking;
use App\Utility\Utility;
use App\Message\Message;

$objHotelBooking = new HotelBooking();
$objHotelBooking->setData($_GET);
$singleData = $objHotelBooking->view();
$priceList = $objHotelBooking->priceList();


if(!$singleData){
    Message::message("Search A Valid ID First...");
    Utility::redirect('index.php#bookingInformation');
}
else{
    $bookingId = $singleData->book_id;
    $hotelName = $singleData->book_hotel;
    $location = str_replace("#","'",$singleData->hotel_location);
    $singleRoom = $singleData->single_room;
    $doubleRoom = $singleData->	double_room;
    $suitRoom = $singleData->suit_room;
    $duration = $singleData->Durations;
    $contactNo = $singleData->mobile_no;
    $totalPayment = ($priceList->single_room_price*$singleRoom) + ($priceList->double_room_price*$doubleRoom) + ($priceList->suit_room_price*$suitRoom);

    $tableData = "";

$html =<<<hotelBooking
    <head>
        <!--Bootstrap file link-->
        <link rel="stylesheet" type="text/css" href="../../resources/bootstrap/css/bootstrap.min.css">
        <!--CSS For File-->
        <link rel="stylesheet" type="text/css" href="../../resources/cssFile/file.css">
        <!--Bootstrap file link-->
        <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
    </head>

    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
         <div class="logo">
             <img src="../../resources/images/logoImage/logo.png"/>
         </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p>Booking Details</p>  
    </div>
    
    <hr/>

    <div class="table-responsive">
        <table class="table table-bordered table-striped">
            <tr>
                <th colspan="2" style='background-color: #9d9d9d; color: gainsboro; height: 30px'>
                    &nbsp;&nbsp;General Information :
                </th>
            </tr>
            
            <tr>
               <td>Booking Serial: </td> 
               <td>$bookingId</td> 
            </tr>
            
            <tr>
                <td>Hotel Name: </td>
                <td>$hotelName</td>
            </tr>
            
            <tr>
                <td>Location: </td>
                <td>$location</td>
            </tr>
            
            <tr>
                <th colspan="2"  style='background-color: #9d9d9d; color: gainsboro; height: 30px'>
                    &nbsp;&nbsp;Room Details :
                </th>
            </tr>
            
            <tr>
                <td>Single: </td>
                <td>$singleRoom</td>
            </tr>
            
            <tr>
                <td>Double: </td>
                <td>$doubleRoom</td>
            </tr>
            
            <tr>
                <td>Suit: </td>
                <td>$suitRoom</td>
            </tr>
            
            <tr>
                <th colspan="2" style='background-color: #9d9d9d; color: gainsboro; height: 30px'>
                    &nbsp;&nbsp;Stay Information :
                </th>
            </tr>
            
            <tr>
                <td>Duration: </td>
                <td>$duration days</td>
            </tr>
            
            <tr>
                <th colspan="2" style='background-color: #9d9d9d; color: gainsboro; height: 30px'>
                    &nbsp;&nbsp;Guest Contact :
                </th>
            </tr>
            
            <tr>
                <td>Mobile No: </td>
                <td>$contactNo</td>
            </tr>   
            
            <tr>
                <th colspan="2" style='background-color: #9d9d9d; color: gainsboro; height: 30px'>
                    &nbsp;&nbsp;Payment Information :
                </th>
            </tr>
            
            <tr>
                <td colspan="2">
                    Hello dear! You have to pay $totalPayment BDT only within 24 hours via Robi bikas (01877824786) or MasterCard Or Paypal.<br/>
                    <center>Important: If Robi Bikas you use Please Confirm at 01877824786 by call.</center>
                </td>
            </tr>
            
        </table>
    </div>
    
    <footer>
        <h6>Powered By: Sarada It Solution</h6>
    </footer>
hotelBooking;

//third party class file:
require_once("../../vendor/mpdf/mpdf/mpdf.php");

$mpdf = new mPDF();

$mpdf->WriteHTML($html);

//Output a PDF file directory to the browser: Download file
$mpdf->Output('BookingInformation.pdf','D');

}
