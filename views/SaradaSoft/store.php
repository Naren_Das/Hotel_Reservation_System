<?php
require_once("../../vendor/autoload.php");
use App\Utility\Utility;
use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}

if(isset($_POST['bookNow'])){
    $objHotelBooking = new \App\HotelBooking\HotelBooking();
    $objHotelBooking->setData($_POST);
    if($objHotelBooking->duplicacyCheck()){
        $objHotelBooking->store();
    }
    else{
        Message::message("Sorry! Duplicate Entry...");
    }
}

Utility::redirect('index.php#bookingInformation');