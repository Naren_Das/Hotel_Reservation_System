<?php
require_once("../../vendor/autoload.php");
if(!isset($_SESSION))session_start();

use App\Utility\Utility;
use App\Message\Message;

$msg = Message::message();

$objhotelDetails = new \App\HotelDetails\HotelDetails();
$objhotelBooking = new \App\HotelBooking\HotelBooking();

//****************************************************For All Hotel Details********************************************//
$allData = $objhotelDetails->index();

//****************************************************For Booking Hotel***********************************************//
if(isset($_GET['id'])){
    $objhotelDetails->setData($_GET);
    $singleData = $objhotelDetails->view();

    $hotelId = $_GET['id'];
    $hotelName = $singleData->hotel_name;
    $hotelLocation = $singleData->location;
    $singleRoom = "";
    $doubleRoom = "";
    $suitRoom = "";
    $duration = "";
    $contactNo = "";
}
else{
    $hotelId = "";
    $hotelName = "";
    $hotelLocation = "";
    $singleRoom = "";
    $doubleRoom = "";
    $suitRoom = "";
    $duration = "";
    $contactNo = "";
}

//************************************************Id maker for booking and search form task**********************************************//
if(isset($_POST['SearchID'])){
    $objhotelBooking->setData($_POST);
    $singleData = $objhotelBooking->view();
    if(!$singleData){
        $singleData = $objhotelBooking->serailNo();
        $bookID = $singleData->book_id;

        $hotelName = "";
        $hotelLocation = "";
        $singleRoom = "";
        $doubleRoom = "";
        $suitRoom = "";
        $duration = "";
        $contactNo = "";

        $message = "No ID Found!";
    }
    else{
        $bookID = $singleData->book_id;
        $hotelName = $singleData->book_hotel;
        $hotelLocation = str_replace("#","'",$singleData->hotel_location);
        $singleRoom = $singleData->single_room;
        $doubleRoom = $singleData->double_room;
        $suitRoom = $singleData->suit_room;
        $duration = $singleData->Durations;
        $contactNo = $singleData->mobile_no;
        $message = "";
    }
}
else{
    $singleData = $objhotelBooking->serailNo();
    $bookID = $singleData->book_id;
    $message = "";
}

if(isset($_POST['SendMessage'])){
    $message1 = "Message has been sent! We send a feedback later...";
}
else{
    $message1 = "";
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Navana</title>
        <!--Bootstrap file link-->
        <link rel="stylesheet" type="text/css" href="../../resources/bootstrap/css/bootstrap.min.css">
        <!--Main CSS File-->
        <link rel="stylesheet" type="text/css" href="../../resources/cssFile/style.css">
        <!--Jquery CSS File-->
        <link rel="stylesheet" type="text/css" href="../../resources/cssFile/jquery-ui.css">
    </head>

    <body>
            <!--Header Section-->
            <div class="header navbar-fixed-top">

                <div class="logo">
                    <img src="../../resources/images/logoImage/logo.png"/>
                </div>

                <header>

                    <div class="container">

                        <nav class="site-nav">

                            <ul>

                                <li><a href="#AboutUs"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                                <li><a href="#Gallery"><span class="glyphicon glyphicon-camera"></span> Gallery</a></li>
                                <li><a href="#hotelDetails"><span class="glyphicon glyphicon-object-align-bottom"></span> Hotel Details</a></li>
                                <li><a href="#budget"><span class="glyphicon glyphicon-briefcase"></span> Budget</a></li>
                                <li><a href="#contact"><span class="glyphicon glyphicon-phone-alt"></span> Contact</a></li>

                            </ul>

                        </nav>

                        <div class="menu-toggle">
                            <div class="hamburger"></div>
                        </div>

                    </div>
                </header>
            </div>

            <!--Slider Area-->
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                    <li data-target="#myCarousel" data-slide-to="5"></li>

                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="../../resources/images/slider_image/1.jpg" alt="Chania">
                    </div>

                    <div class="item">
                        <img src="../../resources/images/slider_image/2.jpg" alt="Chania">
                    </div>

                    <div class="item">
                        <img src="../../resources/images/slider_image/3.jpg" alt="Flower">
                    </div>

                    <div class="item">
                        <img src="../../resources/images/slider_image/ctg1.jpg" alt="Flower">
                    </div>

                    <div class="item">
                        <img src="../../resources/images/slider_image/5.png" alt="Flower">
                    </div>

                    <div class="item">
                        <img src="../../resources/images/slider_image/6.jpg" alt="Flower">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <!--Content Section : Home-->
            <div class="home" id="AboutUs">
                <div class="banner">
                    <p>About Us</p>
                </div>

                <div class="aboutContent">
                    <!--Logo-->
                    <div class="logo col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <img src="../../resources/images/logoImage/logo.png"/>
                    </div>

                    <!--Content Message-->
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 aboutText text">
                            <p>
                                is an online reservation system which will work precisely as you need. If you’re close to giving up on finding online reservation software which will work precisely as you need, you've finally found the right scheduling system!
                                Navana is an online reservation system which can be used by any business taking bookings: for days, nights, hours or minutes, or scheduled events.
                                No other online booking system has so many integrations. Our reservation software will integrate with all popular website builders, over 50 payment gateways, countless cloud-based apps such as Google Calendar & Analytics, mailing list managers, CRM and accounting systems. We also have an open API.
                                Save time by automating all reservation tasks: show up-to-date availability and immediate price quotations, ask for any information on the booking form, handle cancellations, modifications and set up automatic confirmations.
                            </p>
                    </div>

                </div>

            </div>

            <!--Content Section: gallery-->
            <div class="home" id="Gallery">

                <div class="banner">
                    <p>Gallery</p>
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/ctg3.jpg"/>
                    The Cox Today
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/Cox2.jpg"/>
                    Long Beach Hotel
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/bban.jpg"/>
                    Resort Beach View
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/ctg1.jpg"/>
                    Vista Bay Resort
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/ctgf2.jpg"/>
                    The Peninsula Chittagong Limited
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/ctgF1.jpeg"/>
                    Hotel Agrabad
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/ctgf3.jpg"/>
                    Hotel Lord's Inn
                </div>

                <div class="gallery col-lg-3 col-md-3 col-sm-4">
                    <img src="../../resources/images/gallery_image/ctgf4.jpg"/>
                    Hotel Diamond Park
                </div>

            </div>

            <!--Content Section: Hotel Details-->
            <div class="home" id="hotelDetails">

                <div class="banner">
                    <p>Hotel Details</p>
                </div>

                <div id="accordion" class="hotelDetails">

                    <h3 style="background-color: #31b0d5; font-family: 'Comic Sans MS';">Cox's Bazar</h3>

                      <div>
                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                              <a href="#hotelDetails" id="coxTodayLink">The Cox Today</a>
                              <a href="#hotelDetails" id="longBeachLink">Long Beach Hotel</a>
                              <a href="#hotelDetails" id="resortBeachLink">Resort Beach View</a>
                              <a href="#hotelDetails" id="vistaBayLink">Vista Bay Resort</a>
                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="coxToday">
                              <h2>The Cox Today</h2>
                              <h5>Kolatoli Road, N110, Cox's Bazar 4700, Bangladesh.</h5>
                              <hr/>
                              <img src="../../resources/images/HotelOverview/TheCoxToday.jpg" class="firstImage">
                              <p class="text col-sm12 col-xs-12">
                                  The Cox Today Limited is a 4-star property in Port City's upscale GEC Circle, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  The Cox Today Limited is a 20-minute drive from Kolatoli Bus Station. Cox's Bazar International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Cox's Bazar! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1CoxToday.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2CoxToday.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3CoxToday.PNG"/>
                              </div>

                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="longBeach" style="display: none;">
                              <h2>Long Beach Hotel</h2>
                              <h5>14 Kolatoli Road, Cox's Bazar, Bangladesh.</h5>
                              <hr/>
                              <img src="../../resources/images/gallery_image/Cox2.jpg" class="firstImage">
                              <p class="text">
                                  The Long Beach Hotel is a 4-star property in Cox's Bazar, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  The Long Beach Hotel is a 20-minute drive from Kolatoli Bus Station. Cox's Bazar International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Cox's Bazar! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1LongBeach.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2LongBeach.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3LongBeach.PNG"/>
                              </div>
                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="resortBeach" style="display: none;">
                              <h2>Resort Beach View</h2>
                              <h5>Kolatoli Road - 10, Cox's Bazar 4700, Bangladesh.</h5>
                              <hr/>
                              <img src="../../resources/images/gallery_image/bban.jpg" class="firstImage">
                              <p class="text">
                                  The Resort Beach View hotel is a 4-star property in Cox's Bazar, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  Resort Beach View Hotel is a 20-minute drive from Kolatoli Bus Station. Cox's Bazar International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Cox's Bazar! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1ResortBeachView.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2ResortBeachView.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3.ResortBeachView.PNG"/>
                              </div>

                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="vistaBay" style="display: none;">
                              <h2>Vista Bay Resort</h2>
                              <h5>Kolatoli Road - 16, Cox's Bazar 4700, Bangladesh.</h5>
                              <hr/>
                              <img src="../../resources/images/gallery_image/ctg1.jpg" class="firstImage">
                              <p class="text">
                                  The Vista Bay Resort is a 4-star property in Cox's Bazar, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  Vista Bay Resort is a 20-minute drive from Kolatoli Bus Station. Cox's Bazar International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Cox's Bazar! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1VistaBay.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2VistaBay.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3.VistaBay.PNG"/>
                              </div>
                          </div>

                      </div>


                    <h3 style="background-color: #31b0d5; font-family: 'Comic Sans MS';">Chittagong</h3>

                      <div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <a href="#hotelDetails" id="peninsulaLink">The Peninsula Chittagong</a>
                            <a href="#hotelDetails" id="agrabadLink">Hotel Agrabad</a>
                            <a href="#hotelDetails" id="lordInLink">Hotel Lord's Inn</a>
                            <a href="#hotelDetails" id="diamondParkLink">Hotel Diamond Park</a>
                        </div>

                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="peninsula">
                              <h2>The Peninsula Chittagong Limited</h2>
                              <h5>Bulbul Centre, 486/B, O.R.Nizam Road, CDA Avenue, 4000 Chittagong, Bangladesh </h5>
                              <hr/>
                              <img src="../../resources/images/gallery_image/ctgf2.jpg" class="firstImage">
                              <p class="text">
                                  The The Peninsula Chittagong Limited is a 4-star property in Port City's upscale GEC Circle, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  The The Peninsula Chittagong Limited is a 20-minute drive from Alangkar Bus Station. Chittagong International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Cox's Bazar! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1Peninsula.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2Peninsula.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3Peninsula.PNG"/>
                              </div>

                          </div>


                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="hotelAgrabad" style="display: none;">
                              <h2>Hotel Agrabad</h2>
                              <h5>1672 Sabder Ali Road, Agrabad C/A, Agrabad, 4000 Chittagong, Bangladesh</h5>
                              <hr/>
                              <img src="../../resources/images/gallery_image/ctgF1.jpeg" class="firstImage">
                              <p class="text">
                                  The Hotel Agrabad is a 4-star property in Cox's Bazar, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  Hotel Agrabad is a 20-minute drive from Garibulla majar Bus Station. Chittagong International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Chittagong! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1Agrabad.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2Agrabad.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3Agrabad.PNG"/>
                              </div>
                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="lordsInn" style="display: none;">
                              <h2>Hotel Lord's Inn</h2>
                              <h5>Hosna Kalam Coplex, Chittagong, Bangladesh. 4000 Chittagong, Bangladesh</h5>
                              <hr/>
                              <img src="../../resources/images/gallery_image/ctgf3.jpg" class="firstImage">
                              <p class="text">
                                  The Hotel Lord's Inn is a 4-star property in Cox's Bazar, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  Hotel Lord's Inn is a 20-minute drive from Garibulla majar Bus Station. Chittagong International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Chittagong! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1lordsInn.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2lordsInn.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3lordsInn.PNG"/>
                              </div>
                          </div>

                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="diamondPark" style="display: none;">
                              <h2>Hotel Diamond Park</h2>
                              <h5>Khaja Super Market,South side of Police Box, 4000 Chittagong, Bangladesh.</h5>
                              <hr/>
                              <img src="../../resources/images/gallery_image/ctgf4.jpg" class="firstImage">
                              <p class="text">
                                  The Hotel Diamond Park is a 4-star property in Cox's Bazar, a short 5-minute walk from Central Plaza shopping area. An outdoor pool, pampering spa treatments and a well-equipped fitness centre are available. There is also a 24-hour front desk and free parking.
                                  The air-conditioned guestrooms all come with a flat-screen TV, minibar and personal safe. Separate smoking and non-smoking floors are available. En suite bathrooms are equipped with a shower.
                                  Hotel Diamond Park is a 20-minute drive from Garibulla majar Bus Station. Chittagong International Airport is a 45-minute drive away.
                                  Laguna Multi-Cuisine Restaurant offers dishes from Bangladesh, India, Thailand and China. Other dining options include Continental and Korean food at Flamingo Cafe, pastries at Orchid Patisserie and cocktails at Isles Bar.
                                  At the spa, guests can relax with a massage, or at the sauna and hot tub. The hotel also provides a business centre and a tour desk.
                                  Couples particularly like the location they rated it 8 for a two-person trip.
                                  This property is also rated for the best value in Chittagong! Guests are getting more for their money when compared to other properties in this city.
                                  We speak your language!
                              </p>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/1DiamondPark.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/2DiamondPark.PNG"/>
                              </div>

                              <div class="gallery col-lg-4 col-md-4 col-sm-4">
                                  <img src="../../resources/images/HotelOverview/3DiamondPark.PNG"/>
                              </div>
                          </div>

                      </div>

                </div>

            </div>

            <!--Budget Wise Hotel Search-->
            <div class="home">

                <div class="banner" id="budget">
                    <p>Budget</p>
                </div>

                <div class="budget col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-bordered">

                        <caption style="text-align: center; font-family: 'Comic Sans MS';"><h5 style="color: #28a4c9;">Find Your Room..<hr/></h5></caption>

                        <tr>
                            <th>Serial No.</th>
                            <th>Hotel Name</th>
                            <th>Location</th>
                            <th>Room Details</th>
                            <th>Available Rooms</th>
                            <th>Action</th>
                        </tr>

                        <?php
                            foreach ($allData as $record){
                                $location = str_replace("#","'",$record->location);
                                echo "
                                    <tr>
                                        <td>$record->h_ID.</td>
                                        <td>$record->hotel_name</td>
                                        <td>$location</td>
                                        <td>Single: $record->single_room_price BDT <hr/>Double: $record->double_room_price BDT <hr/>Suit: $record->suit_room_price BDT </td>
                                        <td>Single Room: $record->single_room <hr/>Double Room: $record->double_room <hr/>Suit: $record->suit_room </td>
                                        <td><a href='index.php?id=$record->h_ID#booking' class='btn btn-primary btn-sm'><span class='glyphicon glyphicon-tag'></span> Book Now</a></td>
                                    </tr>
                                ";
                            }
                        ?>

                    </table>

                </div>

                <div class="budget col-lg-12 col-md-12 col-sm-12 col-xs-12" id="bookingInformation">
                    <h5 style="text-align: center; font-family: 'Comic Sans MS'; color: #28a4c9;">Enter Your Information<hr/></h5>

                        <!--For Search field-->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <?php
                                   echo"
                                    <span style='color: red; border-radius: 4px;' id='message'>$msg</span>
                                  ";
                                ?>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <p style="color: red;" id="errorMessage"><?php echo $message?></p>
                                <form action="index.php#bookingInformation" method="post" id="searchDiv">
                                    <input class="form-control no-sppiners" type="number" value="" name="serialNo" placeholder="Search Your Booking ID...." id="searchBox" required="required"/>
                                    <input class="btn btn-success btn-sm" type="submit" style="margin-top: 3px" value="Search" name="SearchID" data-toggle='tooltip' data-placement='right' title='Click to search!'/>
                                </form>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container" id="booking">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-2"></div>

                            <div class="col-lg-7 col-md-7 col-sm-7">

                                <form action="store.php" method="post">

                                    <div class="form-group">
                                        <label for="serialNo">Booking ID: <span style="color: red">(Remember It !)</span></label>
                                        <input type="text" class="form-control form2Hover" name="serialNo" value="<?php echo $bookID?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="hotelName">Hotel Name: </label>
                                        <input type="text" class="form-control form2Hover" name="hotelName" placeholder="Please hit the Book Now..." value="<?php echo $hotelName?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="location">Location: </label>
                                        <textarea class="form-control form2Hover" name="location" rows="6" cols="20" placeholder="Please hit the Book Now..."><?php echo $hotelLocation?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="requiredRoom">Required Room: </label><br/>
                                        <label>Single: </label><input type="text" id="singleRoom" name="singleRoom" class="form-control form2Hover" placeholder="Enter amount of rooms..." value="<?php echo $singleRoom?>"/>
                                        <label>Double: </label><input type="text" id="doubleRoom" name="doubleRoom" class="form-control form2Hover" placeholder="Enter amount of rooms..." value="<?php echo $doubleRoom?>"/>
                                        <label>Suit: </label><input type="text" id="suitRoom" name="suitRoom" class="form-control form2Hover" placeholder="Enter amount of rooms..." value="<?php echo $suitRoom?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="duration">Duration:</label>
                                        <input type="text" id="bookingDuration" name="bookingDuration" class="form-control form2Hover" placeholder="How Many Days (Numeric Only...)" required="required" value="<?php echo $duration?>"/>
                                    </div>

                                    <div class="form-group">
                                        <label for="contact">Contact No.</label>
                                        <input type="text" name="contactNo" class="form-control form2Hover" placeholder="Enter a valid contact number..." required="required" value="<?php echo $contactNo?>"/>
                                    </div>

                                    <input type="hidden" name="hotelId" value="<?php echo $hotelId ?>"/>
                                    <input type="hidden" name="currentDateTime" value="<?php date_default_timezone_set('Asia/Dhaka'); echo date("Y-m-d h:i:sa")?>">

                                    <div class="form-group">
                                        <input type="submit" name="bookNow" value="Submit" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="right" title="Book Your Rooms Now!"/>
                                        <button type="reset" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Reset Your Information">Refresh</button>
                                        <?php
                                           echo "
                                                <a href='pdf.php?serialNo=$bookID' class='btn btn-primary btn-sm' data-toggle='tooltip' data-placement='right' title='Download Your Booking Receieve!'>Download PDF</a>
                                           ";
                                        ?>
                                    </div>

                                </form>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>
                        </div>
                </div>

            </div>

            <!--Contact Section-->
            <div class="home" style="width:100%;" id="contact">

                <div class="banner">
                    <p>Contact</p>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height: 20px;">
                    <p style="color: red;" id="message1"><?php echo $message1?></p>
                </div>

                <div style="margin-top: 30px;" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 container">

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>

                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <h3 style="text-align: center; color: #31b0d5; font-family: 'Comic Sans MS';">Get In Touch</h3>
                        <form action="index.php#contact" class="form-horizontal" method="post">

                            <div class="form-group">
                                <label class="whitefont">Your Name</label>
                                <input type="text"  placeholder="Your Name..." class="form-control form2Hover" name="name" id="name" required="required">
                            </div>

                            <div class="form-group">
                                <label class="whitefont">Email Address</label>
                                <input type="text" placeholder="Enter Your Email Address..." class="form-control form2Hover" name="email" id="email" required="required">
                            </div>

                            <div class="form-group">
                                <label class="whitefont">Subject</label>
                                <input type="text" placeholder="Subject..." class="form-control form2Hover" name="subject" id="subject" required="required">
                            </div>

                            <div class="form-group">
                                <label  class="whitefont">Your Message</label>
                                <textarea placeholder="Please Type Your Message..." class="form-control form2Hover" name="message" id="message" rows="3" required="required"></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" name="SendMessage" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="right" title="Send Your Message!">Submit</button>
                                <button type="reset" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="right" title="Reset Your Information">Refresh</button>
                            </div>

                        </form>
                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"></div>

                </div>

                <hr class="col-lg-11 col-md-11 col-sm-11 col-xs-11" style="margin-left: 3%"/>
            </div>

            <div>
                <footer>
                    <h5>Developed By: Sarada IT Solution.</h5>
                    <h6>All Rights Reserved</h6>
                </footer>
            </div>

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="../../resources/bootstrap/js/jquery-3.2.0.min.js"></script>
            <!--Bootstrap file link-->
            <script src="../../resources/bootstrap/js/bootstrap.min.js"></script>
            <!--Jquery file link-->
            <script src="../../resources/jsFile/jquery-ui.js"></script>
            <!--My Own Js File Link-->
            <script src="../../resources/jsFile/own.js"></script>

            <script>
                $( "#accordion" ).accordion();

                //For Hotel Details Section:
                $(document).ready(function () {

                        $('#coxTodayLink').click(function () {
                            if($('#longBeach').is(':visible') || $('#resortBeach').is(':visible') || $('#vistaBay').is(':visible')){

                                $('#coxToday').hide();
                                $('#longBeach').hide();
                                $('#resortBeach').hide();
                                $('#vistaBay').hide();
                                $('#coxToday').fadeIn(1500);
                                $('#coxToday').show();

                            }
                        });

                        $('#longBeachLink').click(function () {
                            if($('#coxToday').is(':visible') || $('#resortBeach').is(':visible') || $('#vistaBay').is(':visible')){

                                $('#longBeach').hide();
                                $('#coxToday').hide();
                                $('#resortBeach').hide();
                                $('#vistaBay').hide();
                                $('#longBeach').fadeIn(1500);
                                $('#longBeach').show();

                            }
                        });

                        $('#resortBeachLink').click(function () {
                            if($('#coxToday').is(':visible') || $('#longBeach').is(':visible') || $('#vistaBay').is(':visible')){

                                $('#resortBeach').hide();
                                $('#coxToday').hide();
                                $('#longBeach').hide();
                                $('#vistaBay').hide();
                                $('#resortBeach').fadeIn(1500);
                                $('#resortBeach').show();
                            }
                        });

                        $('#vistaBayLink').click(function () {
                            if($('#coxToday').is(':visible') || $('#longBeach').is(':visible') || $('#resortBeach').is(':visible')){

                                $('#vistaBay').hide();
                                $('#coxToday').hide();
                                $('#longBeach').hide();
                                $('#resortBeach').hide();
                                $('#vistaBay').fadeIn(1500);
                                $('#vistaBay').show();
                            }
                        });
                });


                $(document).ready(function () {

                    $('#peninsulaLink').click(function () {
                        if($('#hotelAgrabad').is(':visible') || $('#lordsInn').is(':visible') || $('#diamondPark').is(':visible')){

                            $('#peninsula').hide();
                            $('#hotelAgrabad').hide();
                            $('#lordsInn').hide();
                            $('#diamondPark').hide();
                            $('#peninsula').fadeIn(1500);
                            $('#peninsula').show();

                        }
                    });

                    $('#agrabadLink').click(function () {
                        if($('#peninsula').is(':visible') || $('#lordsInn').is(':visible') || $('#diamondPark').is(':visible')){

                            $('#hotelAgrabad').hide();
                            $('#peninsula').hide();
                            $('#lordsInn').hide();
                            $('#diamondPark').hide();
                            $('#hotelAgrabad').fadeIn(1500);
                            $('#hotelAgrabad').show();

                        }
                    });

                    $('#lordInLink').click(function () {
                        if($('#hotelAgrabad').is(':visible') || $('#peninsula').is(':visible') || $('#diamondPark').is(':visible')){

                            $('#lordsInn').hide();
                            $('#hotelAgrabad').hide();
                            $('#peninsula').hide();
                            $('#diamondPark').hide();
                            $('#lordsInn').fadeIn(1500);
                            $('#lordsInn').show();

                        }
                    });

                    $('#diamondParkLink').click(function () {
                        if($('#hotelAgrabad').is(':visible') || $('#peninsula').is(':visible') || $('#lordsInn').is(':visible')){

                            $('#diamondPark').hide();
                            $('#lordsInn').hide();
                            $('#hotelAgrabad').hide();
                            $('#peninsula').hide();
                            $('#diamondPark').fadeIn(1500);
                            $('#diamondPark').show();

                        }
                    });

                });


                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })

                $('#message').fadeToggle(550);
                $('#message').fadeToggle(550);
                $('#message').fadeToggle(550);
                $('#message').fadeToggle(550);
                $('#message').fadeToggle(550);
                $('#message').fadeToggle(550);
                $('#message').fadeOut(550);

                $('#message1').fadeToggle(550);
                $('#message1').fadeToggle(550);
                $('#message1').fadeToggle(550);
                $('#message1').fadeToggle(550);
                $('#message1').fadeToggle(550);
                $('#message1').fadeToggle(550);
                $('#message1').fadeOut(550);
            </script>
    </body>
</html>
