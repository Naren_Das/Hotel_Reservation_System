<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit3799fc753743f68802fd8dddf7237aca
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src/SaradaSoft',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit3799fc753743f68802fd8dddf7237aca::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit3799fc753743f68802fd8dddf7237aca::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
