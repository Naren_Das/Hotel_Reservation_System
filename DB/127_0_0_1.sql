-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2017 at 03:37 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel_reservation`
--
CREATE DATABASE IF NOT EXISTS `hotel_reservation` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `hotel_reservation`;

-- --------------------------------------------------------

--
-- Table structure for table `tb_booking`
--

CREATE TABLE `tb_booking` (
  `book_id` int(200) NOT NULL,
  `book_hotel` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `hotel_location` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `single_room` int(11) NOT NULL DEFAULT '0',
  `double_room` int(11) NOT NULL DEFAULT '0',
  `suit_room` int(11) NOT NULL DEFAULT '0',
  `Durations` int(11) NOT NULL,
  `mobile_no` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `booking_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `h_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_booking`
--

INSERT INTO `tb_booking` (`book_id`, `book_hotel`, `hotel_location`, `single_room`, `double_room`, `suit_room`, `Durations`, `mobile_no`, `booking_time`, `h_ID`) VALUES
(1, 'Hotel Diamond Park', 'Khaja Super Market,South side of Police Box, 4000 Chittagong, Bangladesh.', 1, 0, 0, 1, '01829972834', '2017-07-07 19:07:21', 8),
(2, 'Hotel Lord\'s Inn', 'Hosna Kalam Coplex, 4000 Chittagong, Bangladesh.\r\n\r\n', 1, 2, 0, 2, '01829972834', '2017-07-07 19:07:49', 7),
(3, 'Resort Beach View', 'Kolatoli Road - 10, Cox#s Bazar 4700, Bangladesh.', 2, 0, 0, 1, '12432342', '2017-07-10 02:27:58', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tb_hotel`
--

CREATE TABLE `tb_hotel` (
  `h_ID` int(11) NOT NULL,
  `hotel_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `single_room_price` int(11) NOT NULL,
  `double_room_price` int(11) NOT NULL,
  `suit_room_price` int(11) NOT NULL,
  `single_room` int(11) NOT NULL,
  `double_room` int(11) NOT NULL,
  `suit_room` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_hotel`
--

INSERT INTO `tb_hotel` (`h_ID`, `hotel_name`, `location`, `single_room_price`, `double_room_price`, `suit_room_price`, `single_room`, `double_room`, `suit_room`) VALUES
(1, 'The Cox Today', 'Kolatoli Road, N110, Cox#s Bazar 4700, Bangladesh.', 1500, 3000, 5000, 50, 70, 30),
(2, 'Long Beach Hotel', '14 Kolatoli Road, Cox#s Bazar, Bangladesh.', 2000, 4000, 7000, 40, 50, 40),
(3, 'Resort Beach View', 'Kolatoli Road - 10, Cox#s Bazar 4700, Bangladesh.', 1500, 2500, 6500, 20, 20, 30),
(4, 'Vista Bay Resort', 'Kolatoli Road - 16, Cox#s Bazar 4700, Bangladesh.', 1000, 2200, 4500, 25, 30, 20),
(5, 'The Peninsula Chittagong Limited', 'Bulbul Centre, 486/B, O.R.Nizam Road, CDA Avenue, 4000 Chittagong, Bangladesh.', 2000, 3000, 5000, 40, 35, 20),
(6, 'Hotel Agrabad', '1672 Sabder Ali Road, Agrabad C/A, Agrabad, 4000 Chittagong, Bangladesh.', 1100, 2200, 4500, 20, 20, 30),
(7, 'Hotel Lord\'s Inn', 'Hosna Kalam Coplex, 4000 Chittagong, Bangladesh.\r\n\r\n', 900, 1800, 2500, 25, 30, 30),
(8, 'Hotel Diamond Park', 'Khaja Super Market,South side of Police Box, 4000 Chittagong, Bangladesh.', 750, 1500, 2200, 25, 30, 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD PRIMARY KEY (`book_id`),
  ADD KEY `h_ID` (`h_ID`);

--
-- Indexes for table `tb_hotel`
--
ALTER TABLE `tb_hotel`
  ADD PRIMARY KEY (`h_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_hotel`
--
ALTER TABLE `tb_hotel`
  MODIFY `h_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_booking`
--
ALTER TABLE `tb_booking`
  ADD CONSTRAINT `tb_booking_ibfk_1` FOREIGN KEY (`h_ID`) REFERENCES `tb_hotel` (`h_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
